/**
 *
 * Copyright 2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.patch;

import it.bewares.json.JSONFactory;
import it.bewares.json.patch.exception.JSONOperationResolveException;
import it.bewares.json.query.JSONPointer;
import it.bewares.json.tree.JSONArray;
import it.bewares.json.tree.JSONObject;
import it.bewares.json.tree.JSONString;
import it.bewares.json.tree.JSONStructure;
import it.bewares.json.tree.JSONValue;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import lombok.Getter;
import lombok.Setter;

/**
 * A {@code JSONOperation} is one of the fundamental Operations on which a JSON
 * Patch builds.
 *
 * JSON Patch refers to the JavaScript Object Notation Patch in its definition
 * by the IETF in RFC 6902.
 *
 * JSON refers to the JavaScript Object Notation Data Interchange Format in its
 * definition by the IETF in RFC 8259.
 *
 * @author Markus Goellnitz
 * @see <a href="https://tools.ietf.org/html/rfc6902">RFC 6902</a>
 * @see <a href="https://tools.ietf.org/html/rfc8259">RFC 8259</a>
 */
public class JSONOperation {

    public static enum Type {

        add("path", "value"),
        remove("path"),
        replace("path", "value"),
        move("from", "path"),
        copy("from", "path"),
        test("path", "value");

        @Getter
        private String[] arguments;

        Type(String... arguments) {
            this.arguments = arguments;
        }
    }

    @Inject
    private JSONFactory JSON;

    @Getter
    @Setter
    private JSONOperation.Type type;

    @Getter
    private Map<String, Object> arguments;

    /**
     * Allocates a {@code JSONOperation} by default without any arguments nor
     * the type set yet.
     */
    public JSONOperation() {
        this.setArguments(new HashMap<>());
        this.JSON = new JSONFactory();
    }

    /**
     * Allocates a {@code JSONOperation} with a specified string representing
     * the type and specified arguments.
     *
     * @param type the type of the operation represented by a string
     * @param arguments the argument for the operation
     */
    public JSONOperation(String type, Map<String, Object> arguments) {
        this(JSONOperation.Type.valueOf(type), arguments);
    }

    /**
     * Allocates a {@code JSONOperation} with a specified type and specified
     * arguments.
     *
     * @param type the type of the operation
     * @param arguments the argument for the operation
     */
    public JSONOperation(JSONOperation.Type type, Map<String, Object> arguments) {
        this();

        this.setType(type);
        this.setArguments(arguments);
    }

    /**
     * Allocates a {@code JSONOperation} which is represented by the JSON
     * {@code input}.
     *
     * @param input json object to be converted to the corresponding operation
     */
    public JSONOperation(JSONObject input) {
        this(((JSONString) input.get("op")).getValue(), new HashMap<String, Object>() {
            {
                for (String argument : JSONOperation.Type.valueOf(((JSONString) input.get("op")).getValue()).getArguments()) {
                    if ("path".equals(argument) || "from".equals(argument)) {
                        put(argument, new JSONPointer(((JSONString) input.get(argument)).getValue()));
                    } else if ("value".equals(argument)) {
                        put(argument, input.get(argument));
                    }
                }
            }
        });
    }

    /**
     * Set the arguments from the specified argument map.
     *
     * @param arguments argument map
     */
    public void setArguments(Map<String, Object> arguments) throws IllegalArgumentException {
        this.arguments = new HashMap<>();

        for (String key : arguments.keySet()) {
            this.setArgument(key, arguments.get(key));
        }
    }

    /**
     * Set the specified argument value with the specified key.
     *
     * @param key value of the argument
     * @param value key of the argument
     */
    public void setArgument(String key, Object value) throws IllegalArgumentException {
        if ((("path".equals(key) || "from".equals(key)) && !(value instanceof JSONPointer))
                || ("value".equals(key) && !(value instanceof JSONValue))) {
            throw new IllegalArgumentException();
        }

        this.arguments.put(key, value);
    }

    /**
     * Execute this operation on the specified {@code JSONValue}.
     *
     * @param json the json value on which this operation shall be executed
     */
    public void executeOn(JSONValue json) throws JSONOperationResolveException, IllegalArgumentException {
        JSONPointer from;
        JSONPointer path = (JSONPointer) this.arguments.get("path");
        JSONValue value;
        JSONStructure parent = (JSONStructure) JSON.find(path.getParentPointer(), json);
        String key = path.getPath()[path.getPath().length - 1];

        switch (type) {
            case add:
                if (parent instanceof JSONObject) {
                    JSONObject object = (JSONObject) parent;
                    if (!(object.containsKey(key))) {
                        object.put(key, (JSONValue) this.arguments.get("value"));
                    }
                } else if (parent instanceof JSONArray) {
                    JSONArray array = (JSONArray) parent;
                    array.add(Integer.parseInt(key), (JSONValue) this.arguments.get("value"));
                }
                break;

            case remove:
                if (parent instanceof JSONObject) {
                    if (((JSONObject) parent).containsKey(key)) {
                        ((JSONObject) parent).remove(key);
                    } else {
                        throw new JSONOperationResolveException();
                    }
                } else if (parent instanceof JSONArray) {
                    int index = Integer.parseInt(key);
                    if (0 <= index && index < ((JSONArray) parent).size()) {
                        ((JSONArray) parent).remove(index);
                    } else {
                        throw new JSONOperationResolveException();
                    }

                }
                break;

            case replace:
                new JSONOperation(JSONOperation.Type.remove, new HashMap<String, Object>() {
                    {
                        put("path", path);
                    }
                }).executeOn(json);
                new JSONOperation(JSONOperation.Type.add, new HashMap<String, Object>() {
                    {
                        put("path", path);
                        put("value", arguments.get("value"));
                    }
                }).executeOn(json);
                break;

            case move:
                from = (JSONPointer) arguments.get("from");
                value = JSON.find(from, json);
                new JSONOperation(JSONOperation.Type.remove, new HashMap<String, Object>() {
                    {
                        put("path", from);
                    }
                }).executeOn(json);
                new JSONOperation(JSONOperation.Type.add, new HashMap<String, Object>() {
                    {
                        put("path", path);
                        put("value", value);
                    }
                }).executeOn(json);
                break;

            case copy:
                from = (JSONPointer) arguments.get("from");
                value = JSON.find(from, json);
                new JSONOperation(JSONOperation.Type.add, new HashMap<String, Object>() {
                    {
                        put("path", path);
                        put("value", value);
                    }
                }).executeOn(json);
                break;

            case test:
                if (!JSON.find(path, json).equals(this.arguments.get("value"))) {
                    throw new JSONOperationResolveException();
                }
                break;

            default:
                throw new IllegalArgumentException();
        }
    }

}
