/**
 *
 * Copyright 2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.patch.test;

import it.bewares.json.patch.JSONOperation;
import it.bewares.json.patch.exception.JSONOperationResolveException;
import it.bewares.json.query.JSONPointer;
import it.bewares.json.tree.JSONNumber;
import it.bewares.json.tree.JSONObject;
import it.bewares.json.tree.JSONString;
import it.bewares.json.tree.JSONValue;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.testng.Assert;
import org.testng.Assert.ThrowingRunnable;
import org.testng.annotations.Test;

/**
 * This test class is to test the JSONOperation.
 *
 * @author Markus Goellnitz
 */
public class OperationTest {

    @Test
    private void instanciation() {
        JSONOperation op = new JSONOperation(JSONOperation.Type.add, new HashMap<>());
        Assert.assertEquals(op.getArguments(), new HashMap<>());
        Assert.assertEquals(op.getType(), JSONOperation.Type.add);

        Map args2 = new HashMap<String, Object>() {
            {
                put("path", new JSONPointer());
            }
        };
        op = new JSONOperation("remove", args2);
        Assert.assertEquals(op.getArguments(), args2);
        Assert.assertEquals(op.getType(), JSONOperation.Type.remove);

        op = new JSONOperation();
        Assert.assertEquals(op.getArguments(), new HashMap<>());
        Assert.assertEquals(op.getType(), null);

        op = new JSONOperation(new JSONObject<JSONString>() {
            {
                put("op", "copy");
                put("from", "/results/0");
                put("path", "/0");
            }
        });
        Assert.assertEquals(op.getType(), JSONOperation.Type.copy);
        Assert.assertEquals(op.getArguments(), new HashMap<String, Object>() {
            {
                put("from", new JSONPointer("/results/0"));
                put("path", new JSONPointer("/0"));
            }
        });

        op = new JSONOperation(new JSONObject<JSONString>() {
            {
                put("op", "test");
                put("path", "/results/2");
                put("value", new JSONNumber("3"));
            }
        });
        Assert.assertEquals(op.getType(), JSONOperation.Type.test);
        Assert.assertEquals(op.getArguments(), new HashMap<String, Object>() {
            {
                put("path", new JSONPointer("/results/2"));
                put("value", new JSONNumber("3"));
            }
        });
    }

    @Test
    private void arguments() {
        JSONOperation op = new JSONOperation();

        Assert.assertThrows(IllegalArgumentException.class, new ThrowingRunnable() {

            @Override
            public void run() throws Throwable {
                op.setArgument("path", "/a/0");
            }
        });
        Assert.assertThrows(IllegalArgumentException.class, new ThrowingRunnable() {

            @Override
            public void run() throws Throwable {
                op.setArgument("from", "/a/0");
            }
        });
        Assert.assertThrows(IllegalArgumentException.class, new ThrowingRunnable() {

            @Override
            public void run() throws Throwable {
                op.setArgument("value", "/a/0");
            }
        });

        op.setArgument("path", new JSONPointer());
        op.setArgument("from", new JSONPointer());
        op.setArgument("value", new JSONObject());

        Assert.assertEquals(op.getArguments(), new HashMap<String, Object>() {
            {
                put("path", new JSONPointer());
                put("from", new JSONPointer());
                put("value", new JSONObject());
            }
        });
    }

    @Test
    private void add() {
        JSONOperation add = new JSONOperation(JSONOperation.Type.add, new HashMap<String, Object>() {
            {
                put("path", new JSONPointer("/results/6"));
                put("value", new JSONNumber(6));
            }
        });

        JSONValue result = new JSONObject() {
            {
                put("results", Arrays.asList(0, 1, 2, 3, 4, 5));
            }
        };
        add.executeOn(result);
        Assert.assertEquals(result, new JSONObject() {
            {
                put("results", Arrays.asList(0, 1, 2, 3, 4, 5, 6));
            }
        });
    }

    @Test
    private void remove() {
        JSONOperation remove = new JSONOperation(JSONOperation.Type.remove, new HashMap<String, Object>() {
            {
                put("path", new JSONPointer("/results/4"));
            }
        });

        JSONValue result = new JSONObject() {
            {
                put("results", Arrays.asList(0, 1, 2, 3, 4, 5));
            }
        };
        remove.executeOn(result);
        Assert.assertEquals(result, new JSONObject() {
            {
                put("results", Arrays.asList(0, 1, 2, 3, 5));
            }
        });
    }

    @Test
    private void replace() {
        JSONOperation replace = new JSONOperation(JSONOperation.Type.replace, new HashMap<String, Object>() {
            {
                put("path", new JSONPointer("/results"));
                put("value", new JSONObject());
            }
        });

        JSONValue result = new JSONObject() {
            {
                put("results", Arrays.asList(0, 1, 2, 3, 4, 5));
            }
        };
        replace.executeOn(result);
        Assert.assertEquals(result, new JSONObject() {
            {
                put("results", new JSONObject());
            }
        });
    }

    @Test
    private void move() {
        JSONOperation move = new JSONOperation(JSONOperation.Type.move, new HashMap<String, Object>() {
            {
                put("from", new JSONPointer("/results/0"));
                put("path", new JSONPointer("/0"));
            }
        });

        JSONValue result = new JSONObject() {
            {
                put("results", Arrays.asList(0, 1, 2, 3, 4, 5));
            }
        };
        move.executeOn(result);
        Assert.assertEquals(result, new JSONObject() {
            {
                put("0", 0);
                put("results", Arrays.asList(1, 2, 3, 4, 5));
            }
        });
    }

    @Test
    private void copy() {
        JSONOperation copy = new JSONOperation(JSONOperation.Type.copy, new HashMap<String, Object>() {
            {
                put("from", new JSONPointer("/results/0"));
                put("path", new JSONPointer("/0"));
            }
        });

        JSONValue result = new JSONObject() {
            {
                put("results", Arrays.asList(0, 1, 2, 3, 4, 5));
            }
        };
        copy.executeOn(result);
        Assert.assertEquals(result, new JSONObject() {
            {
                put("0", 0);
                put("results", Arrays.asList(0, 1, 2, 3, 4, 5));
            }
        });
    }

    @Test
    private void test() {
        JSONOperation test = new JSONOperation(JSONOperation.Type.test, new HashMap<String, Object>() {
            {
                put("path", new JSONPointer("/results/0"));
                put("value", new JSONNumber(0));
            }
        });

        JSONValue result = new JSONObject() {
            {
                put("results", Arrays.asList(0, 1, 2, 3, 4, 5));
            }
        };
        test.executeOn(result);
        Assert.assertEquals(result, new JSONObject() {
            {
                put("results", Arrays.asList(0, 1, 2, 3, 4, 5));
            }
        });

        test.setArgument("value", new JSONNumber(2));
        Assert.assertThrows(JSONOperationResolveException.class, new ThrowingRunnable() {

            @Override
            public void run() throws Throwable {
                test.executeOn(result);
            }
        });
        Assert.assertEquals(result, new JSONObject() {
            {
                put("results", Arrays.asList(0, 1, 2, 3, 4, 5));
            }
        });
    }

}
